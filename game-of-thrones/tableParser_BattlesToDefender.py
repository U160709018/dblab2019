import pandas as pd

cd = pd.read_csv("./original_data/battles.csv")
t = pd.read_csv("./parsed_data/ikinci_yeni/defenders.csv")

names_in_battles = []
for i in cd.defender_1:
    names_in_battles.append(i)

for i in cd.defender_2:
    if i not in names_in_battles:
        names_in_battles.append(i)

names_in_defenders = []
for i in t.defenders:
    names_in_defenders.append(i)

battle_ids = []
for i in names_in_battles:
    if i in names_in_battles:
        battle_ids.append(names_in_battles.index(i) + 1)

defender_ids = []
for i in names_in_battles:
    if pd.isna(i):
        defender_ids.append("")
    else:
        defender_ids.append(names_in_defenders.index(i) + 1)

raw_data = {"battle_id": battle_ids, "defender_id": defender_ids}

df = pd.DataFrame(raw_data)

print(df)
df.to_csv('./parsed_data/ikinci_yeni/table_headerless_BattlesToDefender.csv',
          index=False,
          header=False)
