from os import listdir
from os.path import isfile, join

CSVFiles = [
    f for f in listdir("parsed_data") if isfile(join("parsed_data", f))
]

for CSVFile in CSVFiles:
    inCSVFile = CSVFile

    inputCsvFile = open("parsed_data/" + inCSVFile, "r")
    outputCsvFile = open(
        "parsed_data/nulled/" + inCSVFile.replace(".csv", "-null.csv"), "w")

    for line in inputCsvFile:
        for index in range(len(line)):
            if line[index] == line[index - 1] == ",":
                outputCsvFile.write("\\N,")
            else:
                outputCsvFile.write(line[index])

    inputCsvFile.close()
    outputCsvFile.close()
