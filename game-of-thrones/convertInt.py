import pandas

df = pandas.read_csv("test.csv")
df.fillna("")
df[["culture"]] = df[["culture"]].astype(int, errors='ignore')

df.to_csv('test2.csv')
