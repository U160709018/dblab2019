use game_of_thrones;

select * from battlestodefender;

INSERT INTO battlestodefender VALUES
    (1,1),
    (2,2),
    (3,1),
    (4,3),
    (5,3),
    (6,3),
    (7,4),
    (8,5),
    (9,5),
    (10,5),
    (11,6),
    (12,5),
    (13,5),
    (14,5),
    (15,3),
    (16,2),
    (17,1),
    (18,3),
    (19,3),
    (20,3),
    (21,3),
    (22,3),
    (23,7),
    (24,5),
    (25,7),
    (26,5),
    (27,8),
    (28,9),
    (28,2),
    (29,6),
    (31,6),
    (32,10),
    (33,10),
    (34,2),
    (35,2),
    (36,1),
    (37,11),
    (38,12),
    (38,13);
    
INSERT INTO battlestoattacker VALUES
    (1,1),
    (2,1),
    (3,1),
    (4,2),
    (5,2),
    (5,3),
    (6,2),
    (6,3),
    (7,1),
    (8,4),
    (9,4),
    (10,4),
    (11,2),
    (12,4),
    (13,4),
    (14,5),
    (14,4),
    (15,2),
    (15,3),
    (16,6),
    (17,1),
    (18,2),
    (19,2),
    (20,6),
    (21,7),
    (22,2),
    (23,8),
    (24,1),
    (25,1),
    (26,9),
    (26,5),
    (27,9),
    (28,10),
    (28,11),
    (28,12),
    (29,5),
    (30,13),
    (31,6),
    (31,14),
    (31,15),
    (31,16),
    (32,4),
    (33,4),
    (34,6),
    (35,6),
    (36,1),
    (36,9),
    (37,17),
    (37,1),
    (38,6),
    (38,14),
    (38,15),
    (38,16);

select * from battletoking;

select * from character;

select * from battlecommanders;
select * from attackercommander;

select * from `character` where character_name = "Roland Crakehall (Lord)";
select * from culture;


INSERT INTO battleCommanders VALUES
    (1,1742),
    (2,959),
    (3,1716),
    (4,2014),
    (5,906),
    (6,1659),
    (7,1782),
    (8,1691),
    (9,1806),
    (10,1819),
    (11,1223),
    (12,1799),
    (13,1519),
    (14,575),
    (15,1752),
    (16,102),
    (17,1458),
    (18,1667),
    (19,1036),
    (20,1834),
    (21,1184),
    (22,1643),
    (23,1493),
    (24,1786),
    (25,1475),
    (26,1821),
    (27,960),
    (28,53),
    (29,1607),
    (30,170),
    (31,114),
    (32,907),
    (33,173),
    (34,1785),
    (35,1684),
    (36,1486),
    (37,1928),
    (38,1038),
    (39,1608),
    (40,1771),
    (41,1743),
    (42,1542),
    (43,1532),
    (44,1160),
    (45,1634),
    (46,1794),
    (47,1851),
    (48,1880),
    (49,1841),
    (50,652),
    (51,1741),
    (52,1960),
    (53,1854),
    (54,1739),
    (55,1718),
    (56,1705),
    (57,1844),
    (58,1546),
    (59,1833),
    (60,1092),
    (61,2015),
    (62,2),
    (63,1937),
    (64,1750),
    (65,1764),
    (66,1682),
    (67,2016),
    (68,1487),
    (69,2017),
    (70,1614),
    (71,1058),
    (72,1790),
    (73,2018);
insert into battlecommanders values
    (74,1826),
    (75,1630),
    (76,507),
    (77,2019),
    (78,1680),
    (79,1882),
    (80,1747);

select * from `character`;

select * from battlecommanders;
select * from attackercommander;
select * from battles;
select * from `character`;

INSERT INTO attackercommander VALUES
    (1,1),
    (3,2),
    (5,3),
    (7,4),
    (9,4),
    (11,4),
    (13,4),
    (14,4),
    (16,5),
    (18,5),
    (10,6),
    (18,6),
    (20,8),
    (22,9),
    (24,10),
    (26,11),
    (27,11),
    (23,13),
    (29,14),
    (24,14),
    (18,15),
    (33,16),
    (35,16),
    (12,17),
    (38,17),
    (3,17),
    (17,17),
    (42,17),
    (44,17),
    (46,18),
    (48,18),
    (50,19),
    (52,19),
    (54,20),
    (55,20),
    (57,20),
    (59,20),
    (35,20),
    (60,21),
    (61,22),
    (60,22),
    (62,26),
    (7,26),
    (63,26),
    (65,28),
    (67,28),
    (69,28),
    (70,28),
    (72,28),
    (73,29),
    (74,30),
    (75,31),
    (76,32),
    (20,32),
    (20,33),
    (58,34),
    (40,34),
    (37,35),
    (77,35),
    (78,36),
    (79,36),
    (1,36),
    (80,37),
    (1,37);

select * from battlecommanders;

use game_of_thrones;
select * from defendercommander;