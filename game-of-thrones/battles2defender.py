import pandas

defenderDf = pandas.read_csv("defenders.csv")
battleDf = pandas.read_csv("battles.csv")

defenderData = defenderDf.to_dict()
defenderDict = {v: k for k, v in defenderData['defenders'].items()}
print(defenderDict)

defenderList = []
battleIDList = []

defenderList.append("aa")
battleIDList.append("aa")

for battleID, def1, def2 in zip(battleDf.battle_number, battleDf.defender_1, battleDf.defender_2):
    if not pandas.isna(def1):
        defenderId = defenderDict[def1] + 1
        defenderList.append(defenderId)
        battleIDList.append(battleID)

    if not pandas.isna(def2):
        defenderId2 = defenderDict[def2] + 1
        defenderList.append(defenderId2)
        battleIDList.append(battleID)
    else:
        continue


raw_data = {
    "battle_id": battleIDList,
    "defender_id": defenderList
}

df = pandas.DataFrame(raw_data)
df.to_csv("battles2Defender.csv")
