import pandas

attackerDf = pandas.read_csv("attacker_commander.csv")
commanderDf = pandas.read_csv("battleCommanders-test.csv")

commanderDf = commanderDf[["character_name", "commander_id"]]
commanderData = commanderDf.set_index("character_name").to_dict()
commanderDict = commanderData["commander_id"]

battleIdList = []
attackerComNameList = []
commanderIdList = []

for attacker, battleId in zip(attackerDf.attacker_commander, attackerDf.battle_id):
    print(attacker)
    if attacker == "Dagmer Cleftjaw":
        attacker = "Dagmer"
    if attacker == "Smalljon Umber":
        attacker = "Jon Umber (Smalljon)"
    if attacker == "Magnar Styr":
        attacker = "Styr"
    if attacker == "Lord Andros Brax":
        attacker = "Andros Brax"
    if attacker == "Dagmer Cleftjaw":
        attacker = "Dagmer"
    if attacker == "Roland Crakehall":
        attacker = "Roland Crakehall (Kingsguard)"
    commanderId = commanderDict[attacker]
    
    commanderIdList.append(commanderId)
    battleIdList.append(battleId)
    attackerComNameList.append(attacker)

raw_data = {
    'commander_id': commanderIdList,
    'battle_id': battleIdList,
}

df = pandas.DataFrame(raw_data)
df.to_csv("final-attackerCommanders.csv")