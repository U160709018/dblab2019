import pandas


housesList = []
cultureList = []
locationsList = []
attackerCommandersList = []
attackerCommanderBattleID = []
defCommanderList = []
defCommanderBattleID = []
attackerList = []
attackerBattleID = []
defenderList = []
defenderBattleID = []

inputFile = pandas.read_csv("character-predictions.csv")
inputFileBattles = pandas.read_csv("battles.csv")

for house in inputFile.house:
    if house in housesList:
        continue
    else:
        if house == "":
            continue
        else:
            housesList.append(house)

# write csv file
raw_data = {
    'house_name': housesList
}

df = pandas.DataFrame(raw_data, columns = ["house_name"])
df.to_csv("house.csv")

# get culture
for culture in inputFile.culture:
    if culture in cultureList:
        continue
    else:
        cultureList.append(culture)

# write culture to csv file
raw_data = {
    'culture_name': cultureList
}

df = pandas.DataFrame(raw_data, columns = ["culture_name"])
df.to_csv("culture.csv")

# get location
nullLocation = inputFileBattles.fillna(" ")
locationsList.append("aa")
for location in nullLocation.location:
    if location in locationsList:
        continue
    else:
        if location == " ":
            continue
        else:
            locationsList.append(location)

# write location to csv file
raw_data = {
    'location': locationsList
}

df = pandas.DataFrame(raw_data, columns = ["location"])
df.to_csv("location.csv")

# get attacker_commander
nullAttackerCommander = inputFileBattles.fillna(" ")
attackerCommandersList.append("aa")
attackerCommanderBattleID.append("aa")
for attackCommander, battleId in zip(nullAttackerCommander.attacker_commander, nullAttackerCommander.battle_number):
    if "," in attackCommander:
        splited = attackCommander.split(",")
        for element in splited:
            if element in attackerCommandersList:
                continue
            else:
                attackerCommandersList.append(element.strip())
                attackerCommanderBattleID.append(battleId)
    else:
        if attackCommander in attackerCommandersList:
            continue
        else:
            if attackCommander == " ":
                continue
            else:
                attackerCommandersList.append(attackCommander)
                attackerCommanderBattleID.append(battleId)

raw_data = {
    'battle_id': attackerCommanderBattleID,
    'attacker_commander': attackerCommandersList
}

df =pandas.DataFrame(raw_data, columns = ["battle_id", "attacker_commander"])
df.to_csv("attacker_commander.csv")

# get defender_commander
nullDefCommander = inputFileBattles.fillna(" ")
defCommanderList.append("aa")
defCommanderBattleID.append("aa")
for defCommander, battleId in zip(nullDefCommander.defender_commander, nullDefCommander.battle_number):
    if "," in defCommander:
        splited = defCommander.split(",")
        for element in splited:
            if element in defCommanderList:
                continue
            else:
                defCommanderList.append(element.strip())
                defCommanderBattleID.append(battleId)
    else:
        if defCommander in defCommanderList:
            continue
        else:
            if defCommander == " ":
                continue
            else:
                defCommanderList.append(defCommander)
                defCommanderBattleID.append(battleId)

raw_data = {
    'battle_id': defCommanderBattleID,
    'defender_commander': defCommanderList
}

df =pandas.DataFrame(raw_data, columns = ["battle_id", "defender_commander"])
df.to_csv("defender_commander.csv")

# attackers
nullAttacker = inputFileBattles.fillna(" ")
attackerList.append("aa")
attackerBattleID.append("aa")
for attacker, attacker2, attacker3, attacker4, battleId in zip(nullAttacker.attacker_1, nullAttacker.attacker_2, nullAttacker.attacker_3, nullAttacker.attacker_4, nullAttacker.battle_number):
    if attacker in attackerList or attacker == "null" or attacker == " ":
        pass
    else:
        attackerList.append(attacker)
        attackerBattleID.append(battleId)
    
    if attacker2 in attackerList or attacker2 == "null" or attacker2 == " ":
        pass
    else:
        attackerList.append(attacker2)
        attackerBattleID.append(battleId)

    if attacker3 in attackerList or attacker3 == "null" or attacker3 == " ":
        pass
    else:
        attackerList.append(attacker3)
        attackerBattleID.append(battleId)
    
    if attacker4 in attackerList or attacker4 == "null" or attacker4 == " ":
        pass
    else:
        attackerList.append(attacker4)
        attackerBattleID.append(battleId)
    
raw_data = {
    'battle_id': attackerBattleID,
    'attackers': attackerList
}

df = pandas.DataFrame(raw_data, columns = ["battle_id", "attackers"])
df.to_csv("attackers.csv")

# defenders
nullDefender = inputFileBattles.fillna(" ")
defenderList.append("aa")
defenderBattleID.append("aa")
for defender, defender2, defender3, defender4, battleId in zip(nullDefender.defender_1, nullDefender.defender_2, nullDefender.defender_3, nullDefender.defender_4, nullDefender.battle_number):
    if defender in defenderList or defender == "null" or defender == " ":
        pass
    else:
        defenderList.append(defender)
        defenderBattleID.append(battleId)
    
    if defender2 in defenderList or defender2 == "null" or defender2 == " ":
        pass
    else:
        defenderList.append(defender2)
        defenderBattleID.append(battleId)

    if defender3 in defenderList or defender3 == "null" or defender3 == " ":
        pass
    else:
        defenderList.append(defender3)
        defenderBattleID.append(battleId)
    
    if defender4 in defenderList or defender4 == "null" or defender4 == " ":
        pass
    else:
        defenderList.append(defender4)
        defenderBattleID.append(battleId)
    
raw_data = {
    'battle_id': defenderBattleID,
    'defenders': defenderList
}

df = pandas.DataFrame(raw_data, columns = ["battle_id", "defenders"])
df.to_csv("defenders.csv")

# battles2defender
nullRemainingBattles = inputFileBattles.fillna("")
defenderSizeList = []
attackerSizeList = []
battleIDList = []
outComeList = []

for dSize, aSize, battleID, outCome in zip(nullRemainingBattles.defender_size, nullRemainingBattles.attacker_size, nullRemainingBattles.battle_number, nullRemainingBattles.attacker_outcome):
    if outCome == "win":
        outComeList.append("w")
    elif outCome == "loss":
        outComeList.append("l")
    else:
        outComeList.append(float('nan'))

    defenderSizeList.append(dSize)
    attackerSizeList.append(aSize)
    battleIDList.append(battleID)

raw_data = {
    "battle_id": battleIDList,
    "defender_size": defenderSizeList,
    "attacker_size": attackerSizeList,
    "attacker_outcome": outComeList
}

df = pandas.DataFrame(raw_data, columns = ["battle_id", "defender_size", "attacker_size", "attacker_outcome"])
df.to_csv("remainingBattles.csv")

# get fixed culture
dontAddCultureList = ["Wildling", "Westermen", "Westerlands", "Vale", "Summer Isles", "Summer Islands", "Stormlands", "Riverlands"
                        "Reach", "Qarth", "Norvos", "Meereen", "Lysene", "Lhazareen", "Ironborn", "Ghiscaricari", "Dornish", "Dorne",
                        "Braavos", "Astapor", "Asshai", "Andals", "westermen", "ironborn", "Riverlands", "The Reach"]
cultureList.clear()
for culture in inputFile.culture:
    if culture in cultureList or culture in dontAddCultureList:
        pass
    elif culture == "Reach":
        continue
    else:
        cultureList.append(culture)

raw_data = {
    "culture": cultureList
}

df = pandas.DataFrame(raw_data, columns = ["culture"])
df.to_csv("cultureUpdated.csv")


# get character
charDf = pandas.read_csv("character-predictions.csv")
houseDf = pandas.read_csv("house-imported.csv")
cultureDf = pandas.read_csv("culture-imported1.csv")

cultureDict = cultureDf.set_index('culture_id').to_dict()
cultureMap = {v: k for k, v in cultureDict['culture_name'].items()}
print(cultureMap)
#print(cultureID)
for charCulture in charDf.culture:
    if pandas.isna(charCulture):
        continue
    if type(charCulture) != type("str"):
        continue
    print(charCulture)
    cultureID = cultureMap[charCulture]
    #print(cultureID)
    charDf.culture.replace([charCulture], cultureID, inplace=True)
print(charDf.culture)