import pandas as pd

cd = pd.read_csv("./original_data/character-deaths.csv")
t = pd.read_csv("./parsed_data/ikinci_yeni/char-deaths-test.csv")

names_in_test = []
for i in t.character_name:
    names_in_test.append(i)

names_in_charDeaths = []
for i in cd.Name:
    names_in_charDeaths.append(i)

char_ids = []
for i in cd.Name:
    if i in names_in_test:
        char_ids.append(names_in_test.index(i) + 1)

cd = cd[[
    'Book of Death', 'Death Year', 'Death Chapter', 'Book Intro Chapter',
    'GoT', 'CoK', 'SoS', 'FfC', 'DwD'
]]
cd = cd.assign(character_id=char_ids)

cd = cd[[
    'Book of Death', 'character_id', 'Death Year', 'Death Chapter',
    'Book Intro Chapter', 'GoT', 'CoK', 'SoS', 'FfC', 'DwD'
]]
print(cd)

cd.to_csv('./parsed_data/ikinci_yeni/table_headerless_characterdeath.csv',
          index=False,
          header=False)
