#replace house

import pandas

df = pandas.read_csv("house-imported.csv")
fullDf = pandas.read_csv("character-full-imported-culture.csv")
dataDict = df.set_index('house_id').to_dict()
dataMap = {v: k for k, v in dataDict['house_name'].items()}
#inputFile = open("character-full-imported-culture-house.csv")
#outputFile = open("character-completed.csv", "a")

for house in fullDf["house"]:
    if pandas.isna(house):
        continue
    if type(house) != type("aa"):
        continue
    if '\"' in house or '\'' in house:
        stripHouse = house.strip('\"')
    else:
        stripHouse = house
    try:
        houseId = dataMap[stripHouse]
    except KeyError:
        continue
    fullDf.house.replace([house], houseId, inplace=True)

fullDf.to_csv("test.csv")
print(fullDf.house)