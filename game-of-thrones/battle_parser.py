#!/usr/bin/env python
# Python 3

import pandas

df = pandas.read_csv("original_data/battles.csv")
df = df[[
    'battle_number', 'name', 'year', 'battle_type', 'region', 'note', 'summer',
    'major_death', 'major_capture'
]]

df.to_csv('parsed_data/ikinci_yeni/table_Battles.csv',
          index=False,
          header=False)
