import pandas

defenderDf = pandas.read_csv("defender_commander.csv")
commanderDf = pandas.read_csv("battleCommanders-test.csv")

commanderDf = commanderDf[["character_name", "commander_id"]]
commanderData = commanderDf.set_index("character_name").to_dict()
commanderDict = commanderData["commander_id"]

battleIdList = []
defenderComNameList = []
commanderIdList = []

for defender, battleId in zip(defenderDf.defender_commander, defenderDf.battle_id):
    print(defender)
    if defender == "Dagmer Cleftjaw":
        defender = "Dagmer"
    if defender == "Smalljon Umber":
        defender = "Jon Umber (Smalljon)"
    if defender == "Magnar Styr":
        defender = "Styr"
    if defender == "Lord Andros Brax":
        defender = "Andros Brax"
    if defender == "Dagmer Cleftjaw":
        defender = "Dagmer"
    if defender == "Roland Crakehall":
        defender = "Roland Crakehall (Kingsguard)"
    commanderId = commanderDict[defender]
    
    commanderIdList.append(commanderId)
    battleIdList.append(battleId)
    defenderComNameList.append(defender)

raw_data = {
    'commander_id': commanderIdList,
    'battle_id': battleIdList,
}

df = pandas.DataFrame(raw_data)
df.to_csv("final-defenderCommanders.csv")