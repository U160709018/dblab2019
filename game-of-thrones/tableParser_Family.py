import pandas as pd

inputFile = pd.read_csv("./parsed_data/ikinci_yeni/merged.csv")

inputFile = inputFile[[
    'S.No', 'mother', 'father', 'heir', 'spouse', 'numDeadRelations',
    'boolDeadRelations'
]]

inputFile.to_csv('./parsed_data/ikinci_yeni/table_headerless_Family1.csv',
                 index=False,
                 header=False)
