import pandas as pd

dir = "./original_data/"
outDir = "./parsed_data/ikinci_yeni/"

files = [dir + "character-predictions.csv", dir + "character-deaths.csv"]

cp_columns = [
    "S.No", "actual", "pred", "alive", "plod", "name", "title", "male",
    "culture", "dateOfBirth", "DateoFdeath", "mother", "father", "heir",
    "house", "spouse", "book1", "book2", "book3", "book4", "book5",
    "isAliveMother", "isAliveFather", "isAliveHeir", "isAliveSpouse",
    "isMarried", "isNoble", "age", "numDeadRelations", "boolDeadRelations",
    "isPopular", "popularity", "isAlive"
]

cd_columns = [
    "Name", "Allegiances", "Death", "Year", "Book of Death", "Death Chapter",
    "Book Intro Chapter", "Gender", "Nobility", "GoT", "CoK", "SoS", "FfC",
    "DwD"
]

df = [pd.read_csv(f) for f in files]

extracted = df[1][['Name', 'Allegiances', 'Gender', 'Nobility']]

names = [i for i in df[0].name]
uniq_names = []
uniq_house = []
uniq_gender = []
uniq_nobility = []
index_left = 1947
for name, house, gender, nobility in zip(extracted.Name, extracted.Allegiances,
                                         extracted.Gender, extracted.Nobility):
    if name not in names:
        uniq_names.append(name)
        uniq_house.append(house)
        uniq_gender.append(gender)
        uniq_nobility.append(nobility)

final_df = df[0]
for i in range(len(uniq_names)):
    final_df = final_df.append(
        {
            "S.No": index_left,
            "name": uniq_names[i],
            "house": uniq_house[i],
            "male": uniq_gender[i],
            "isNoble": uniq_nobility[i]
        },
        ignore_index=True)
    index_left += 1
final_df = final_df.replace('None', float('nan'))
final_df.to_csv(outDir + "merged.csv", index=False, header=True)
