import pandas as pd

df = pd.read_csv("./original_data/battles.csv")

kingz = []
for i in df.attacker_king:
    if not pd.isna(i):
        kingz.append(i)

for i in df.defender_king:
    if i not in kingz:
        if not pd.isna(i):
            kingz.append(i)
kingz = set(kingz)
kingz = list(kingz)

king = []
for i in kingz:
    a = i.split("/")
    if len(a) > 1:
        b = a[1].split()
        c = a[0] + " " + b[1]
        d = b[0] + " " + b[1]
        ind = kingz.index(i)
        kingz.pop(ind)
        kingz.insert(ind, c)
        kingz.insert(ind + 1, d)

raw_data = {"king_id": range(1, len(kingz) + 1), "king_name": kingz}

dataFrame = pd.DataFrame(raw_data)

dataFrame.to_csv("./parsed_data/ikinci_yeni/Kings.csv",
                 index=False,
                 header=False)
print(dataFrame)
