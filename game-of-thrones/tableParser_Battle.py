import pandas

inputFile = pandas.read_csv("./original_data/battles.csv")

inputFile.battle_type.replace(['pitched battle', 'ambush', 'siege', 'razing'],
                              [1, 2, 3, 4],
                              inplace=True)

inputFile.rename(columns={'battle_type': 'battle_type_id'}, inplace=True)

inputFile = inputFile[[
    'battle_number', 'name', 'year', 'battle_type_id', 'region', 'note',
    'summer', 'major_death', 'major_capture'
]]

inputFile.to_csv('./parsed_data/ikinci_yeni/table_headerless_Battles.csv',
                 index=False,
                 header=False)
