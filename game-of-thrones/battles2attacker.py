import pandas

attackerDf = pandas.read_csv("attackers.csv")
battleDf = pandas.read_csv("battles.csv")

attackerData = attackerDf.to_dict()
attackerDict = {v: k for k, v in attackerData['attackers'].items()}
print(attackerDict)

attackerList = []
battleIDList = []

attackerList.append("aa")
battleIDList.append("aa")

for battleID, attacker1, attacker2, attacker3, attacker4 in zip(battleDf.battle_number, battleDf.attacker_1, battleDf.attacker_2, battleDf.attacker_3, battleDf.attacker_4):
    if not pandas.isna(attacker1):
        attackerId = attackerDict[attacker1]
        attackerList.append(attackerId)
        battleIDList.append(battleID)

    if not pandas.isna(attacker2):
        attackerId2 = attackerDict[attacker2]
        attackerList.append(attackerId2)
        battleIDList.append(battleID)
    
    if not pandas.isna(attacker3):
        attackerId3 = attackerDict[attacker3]
        attackerList.append(attackerId3)
        battleIDList.append(battleID)

    if not pandas.isna(attacker4):
        attackerId4 = attackerDict[attacker4]
        attackerList.append(attackerId4)
        battleIDList.append(battleID)


raw_data = {
    "battle_id": battleIDList,
    "attacker_id": attackerList
}

df = pandas.DataFrame(raw_data)
df.to_csv("battles2attacker.csv", index=False)
