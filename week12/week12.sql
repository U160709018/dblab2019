use company;

call selectAllCustomers;

call getCustomersByCity("Barcelona");

select * from Employees where salary = highestSalary();

set @maxSalary = 0;
call highestSalary(@maxSalary);
select * from Employees where salary = @maxSalary;

set @m_count = 0;
call countGender(@m_count, "M");

set @f_count = 0;
call countGender(@f_count, "F");

select @m_count, @f_count;
